package firstpackage;

public class Franchise extends Store implements Cafe {

    @Override
    public void coffee() {
        System.out.println("We have coffee");

    }

    @Override
    public void chicken() {
        System.out.println("We have chicken");
    }

    @Override
    public void groceries() {
        System.out.println("We have groceries");

    }

    @Override
    public void cocaCola() {
        System.out.println("We also have coca cola");

    }

    @Override
    public void hws() {

    }

    @Override
    public void cws() {

    }
}

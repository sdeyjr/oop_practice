package firstpackage;

public interface Cafe extends WhyYouDoThis {

    void coffee ();

    void chicken ();

    void groceries ();

}

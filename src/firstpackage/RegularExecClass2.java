package firstpackage;

public class RegularExecClass2 {
    public static void main(String[] args) {
        RegularClass2 regularClass2 = new RegularClass2();

        regularClass2.lightOrDibba();
        regularClass2.light();
        regularClass2.circulator();
        regularClass2.fan();
        regularClass2.letters();
        regularClass2.lightBulb();
    }
}

package secondpackage;

/*
Built an application for the electric voting system.
 * have 2 candidates predetermined for the vote
 * the application should be able to take x number of peoples names and whom they want to vote for as an input
 * at the end of the application, it should return who is the winner of the vote and how many votes he got
*/

import java.util.Scanner;

public class EVoting {
   public static void main(String[] args) {
      Scanner userInput = new Scanner(System.in);
      final int size = 2;
      int[] votes = new int[size];
      String[] names = new String[size];

      for (int i = 0; i < names.length && i < votes.length; i++) { //&& means and
         System.out.print("Enter candidate's name: ");
         names[i] = userInput.next();
         System.out.print("Enter number of votes: ");
         votes[i] = userInput.nextInt();
      }
      System.out.println("And the Winner is : " + highest(votes, names));
   }

   public static String highest(int[] votes, String[] names) {
      int high = votes[0];
      String s = names[0];
      for (int i = 1; i < votes.length; i++) {
         if (votes[i] > high) {
            high = votes[i];
            s = names[i];
         }
      }
      s = s + " wins with  " + high + " votes";
      return s;
   }
}

package secondpackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class JobiExec {
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        FileInputStream kutta = new FileInputStream("src/firstpackage/Jobi.properties");
        properties.load(kutta);
        String bilai = properties.getProperty("kukur");
        System.out.println(bilai);
    }
}

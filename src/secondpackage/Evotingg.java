package secondpackage;

import java.util.HashMap;
import java.util.Scanner;

public class Evotingg {
    public static void main(String[] args) {
        String candidate1 = "Hasina";
        String candidate2 = "Khaleda";

        HashMap<String, Integer> map = new HashMap<>();
        map.put(candidate1, 0);
        map.put(candidate2, 0);

        Scanner scanner = new Scanner(System.in);
        System.out.println("please insert the number of voters : ");
        int totalVoters = scanner.nextInt();
        System.out.println("total voters number is : " + totalVoters);
        scanner.nextLine();

        for (int i = 0; i < totalVoters; i++) {
            System.out.println("please insert the name of voter : ");
            String nameOfVoters = scanner.nextLine();

            System.out.println("please insert to whom " + nameOfVoters + " wants to vote :");
            String answerFromVoter = scanner.nextLine();

            if (answerFromVoter.equalsIgnoreCase(candidate1)) {
                int previousValue = map.get(candidate1);
                map.put(candidate1, previousValue + 1);
            } else if (answerFromVoter.equalsIgnoreCase(candidate2)) {
                int previousValue = map.get(candidate2);
                map.put(candidate2, previousValue + 1);
            } else {
                System.out.println("please insert correct name of voter");
            }
        }


        System.out.println("total votes of " + candidate1 + " is : " + map.get(candidate1));
        System.out.println("total votes of " + candidate2 + " is : " + map.get(candidate2));


        System.out.print("******SO the winner is : ");

        if (map.get(candidate1) > map.get(candidate2)) {
            System.out.println(candidate1);
        } else if (map.get(candidate2) > map.get(candidate1)) {
            System.out.println(candidate2);
        } else {
            System.out.println("It was a draw ");
        }
    }
}

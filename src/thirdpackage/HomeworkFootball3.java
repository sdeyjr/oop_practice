package thirdpackage;

import java.util.ArrayList;
import java.util.Random;

public class HomeworkFootball3 {

    public static int getRandomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public static ArrayList<String> getRandomNonRepeatingIntegers(int size, int min,
                                                                   int max) {
        ArrayList<String> numbers = new ArrayList<String>();

        while (numbers.size() < size) {
            String random = String.valueOf(getRandomInt(min, max));

            if (!numbers.contains(random)) {
                numbers.add(random);
            }
        }

        return numbers;
    }

    public static void main(String[] args) {
        ArrayList<String> list = getRandomNonRepeatingIntegers(11, 0, 23);
        for (int i = 0; i < list.size(); i++) {
            System.out.println("" + list.get(i));
        }
    }
}

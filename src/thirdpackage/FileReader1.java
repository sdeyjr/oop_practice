package thirdpackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileReader1 {
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream("src/thirdpackage/config4.properties");
        properties.load(fileInputStream);
        String userName = properties.getProperty("username");
        System.out.println(userName);
        String passWord = properties.getProperty("password");
        System.out.println(passWord);
        MyOwnFileReader.getPropertyFile("src/thirdpackage/config4.properties", "value");
    }
}

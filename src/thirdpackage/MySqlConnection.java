package thirdpackage;

import java.io.IOException;
import java.sql.*;

import static thirdpackage.MyOwnFileReader.getPropertyFile;

public class MySqlConnection {
    public static void main(String[] args) throws SQLException, IOException {
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        String username = getPropertyFile("src/thirdpackage/config4.properties", "username");
        String password = getPropertyFile("src/thirdpackage/config4.properties", "password");
        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM products");

        while (resultSet.next()){
            System.out.println(resultSet.getString(3));
        }
    }
}

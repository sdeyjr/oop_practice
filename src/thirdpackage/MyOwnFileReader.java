package thirdpackage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class MyOwnFileReader {
    public static void main(String[] args) throws IOException {
        String username = getPropertyFile("src/thirdpackage/config4.properties", "username");
        String password = getPropertyFile("src/thirdpackage/config4.properties", "password");
        System.out.println(username);
        System.out.println(password);
        String entireFile = getTextFile("src/thirdpackage/Tumar.txt");
        System.out.println(entireFile);
    }

    public static String getPropertyFile(String filepath, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filepath));
        return properties.getProperty(key);
    }

    public static String getTextFile(String filepath) throws IOException {
        StringBuilder finalText = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filepath));
        String tempContainer = "";
        while ((tempContainer = bufferedReader.readLine()) != null) {
            finalText.append(tempContainer);
        }
        return finalText.toString();
    }
}

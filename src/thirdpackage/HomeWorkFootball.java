package thirdpackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* In a soccer team  total players are 23
        for the next match find out the total 11 + 7 (SUB) players by  randomizing
        then from the substitutes, sub in 4 number of players with the beginning 11.
        but you can't sub 3 fixed players.
        At the end print who are the players played full 90 minutes and who got subbed out*/
public class HomeWorkFootball {
    public static void main(String[] args) {
      String[] entireRoster1 = {"Lionel Messi", "Robert Lewandoski", "Christiano Ronaldo", "Kevin De Bruyne", "Kylian Mbappe", "Neymar Jr", "Jan Oblak", "Harry Kane", "N'golo Kante", "Manuel Neuer", "Marc Andre TerStegen", "Mo Salah", "Gianluigi Donaromma", "Karim Benzema", "Virgil Van Dijk", "Joshua Kimmich", "Heung Min Son", "Alison", "Thibaut Courtouis", "Ederson", "Casemiro", "Sadio Mane", "Bernardo Silva" };
        ArrayList<String> entireRoster = new ArrayList<String>();
        entireRoster.add("Lionel Messi");
        entireRoster.add("Robert Lewandowski");
        entireRoster.add("Christiano Ronaldo");
        entireRoster.add("Kevin De Bruyne");
        entireRoster.add("Kylian Mbappe");
        System.out.println(entireRoster);

        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            int playerName = random.nextInt(entireRoster.size());
            System.out.println("Starting 11 : "
                    + entireRoster.get(playerName));
        }
    }
}
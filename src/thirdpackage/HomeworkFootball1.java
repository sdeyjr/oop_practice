package thirdpackage;


// Java program for Getting Random Elements
// from ArrayList using nextInt() function

import java.util.ArrayList;
import java.util.Random;

class HomeworkFootball1 {
    public static void main(String[] args) {
        // creating ArrayList
        ArrayList<String> my_list = new ArrayList<String>();

        // adding elements
        my_list.add("1a");
        my_list.add("2b");
        my_list.add("3c");
        my_list.add("4d");
        my_list.add("5s");
        my_list.add("6f");
        my_list.add("7s");
        my_list.add("8f");

        // initializing random class
        Random random_method = new Random();

        // loop for generation random number
        for (int i = 0; i < 3; i++) {
            // generating random index with the help of
            // nextInt() method
            int index = random_method.nextInt(my_list.size());

            System.out.println("Random Element is :"
                    + my_list.get(index));
        }
    }
}

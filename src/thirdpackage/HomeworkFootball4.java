package thirdpackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/* In a soccer team  total players are 23
        for the next match find out the total 11 + 7 (SUB) players by  randomizing
        then from the substitutes, sub in x number of players with the beginning 11.
        but you can't sub 3 fixed players.
        At the end print who are the players played full 90 minutes and who got subbed out*/

public class HomeworkFootball4 {
    public static void main(String[] args) {
        ArrayList<String> wholeTeam = new ArrayList<>();
        wholeTeam.add("Lionel Messi");
        wholeTeam.add("Christiano Ronaldo");
        wholeTeam.add("Luis Suarez");
        wholeTeam.add("Karim Benzema");
        wholeTeam.add("Neymar Jr");
        wholeTeam.add("Kyliyan Mbappe");
        wholeTeam.add("Robert Lewandoski");
        wholeTeam.add("Kevin De Bruyne");
        wholeTeam.add("Jan Oblak");
        wholeTeam.add("Harry Kane");
        wholeTeam.add("N'golo Kante");
        wholeTeam.add("Manuel Neuer");
        wholeTeam.add("Marc Andre TerStegen");
        wholeTeam.add("Mo Salah");
        wholeTeam.add("Gianluigi Donaromma");
        wholeTeam.add("Virgil Van Dijk");
        wholeTeam.add("Joshua Kimmich");
        wholeTeam.add("Heung Min Son");
        wholeTeam.add("Alison");
        wholeTeam.add("Thibaut Courtouis");
        wholeTeam.add("Ederson");
        wholeTeam.add("Casemiro");
        wholeTeam.add("Sadio Mane");
        System.out.println("Total players at the club : " + wholeTeam.size());

        System.out.println("************************");

        randomTings(wholeTeam);
        for (int i=0; i< wholeTeam.size(); i++){
            System.out.println(wholeTeam.get(i) + " " + i);
        }
        System.out.println("************************");

        ArrayList<String> starting11 = new ArrayList<>();
        starting11.add(wholeTeam.get(0));
        starting11.add(wholeTeam.get(1));
        starting11.add(wholeTeam.get(2));
        starting11.add(wholeTeam.get(3));
        starting11.add(wholeTeam.get(4));
        starting11.add(wholeTeam.get(5));
        starting11.add(wholeTeam.get(6));
        starting11.add(wholeTeam.get(7));
        starting11.add(wholeTeam.get(8));
        starting11.add(wholeTeam.get(9));
        starting11.add(wholeTeam.get(10));
        System.out.println("Starting for today's match are : " + starting11);

        ArrayList<String> substitutePlayers = new ArrayList<>();
        substitutePlayers.add(wholeTeam.get(11));
        substitutePlayers.add(wholeTeam.get(12));
        substitutePlayers.add(wholeTeam.get(13));
        substitutePlayers.add(wholeTeam.get(14));
        substitutePlayers.add(wholeTeam.get(15));
        substitutePlayers.add(wholeTeam.get(16));
        substitutePlayers.add(wholeTeam.get(17));
        System.out.println("And on the bench today : " + substitutePlayers);
        System.out.println("************************");

        starting11.remove(10);
        starting11.remove(9);
        starting11.remove(8);
        starting11.add(substitutePlayers.get(0));
        starting11.add(substitutePlayers.get(1));
        starting11.add(substitutePlayers.get(2));
        System.out.println("Players at the field after substitutiion : " + starting11);
        System.out.println("************************");
        for (int i =0;i<8;i++){
            System.out.println(starting11.get(i) + " played full 90 mins");
        }
    }

    public static void randomTings(List<String> golaso) {

        int n = golaso.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            noDuplication(golaso, i, change);
        }
    }

    public static void noDuplication(List<String> golaso, int i, int offside) {
        String scorpion = golaso.get(i);
        golaso.set(i, golaso.get(offside));
        golaso.set(offside, scorpion);
    }
}
package thirdpackage;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class HomeworkFootball2 {
    public static void main(String[] args) {
        HashMap<Integer, String> songs = new HashMap<>();
        songs.put(1,"Messi");
        songs.put(2, "ronaldo");
        songs.put(3, "Msuarezessi");
        songs.put(4, "benzema");
        songs.put(5, "neyar");
        songs.put(6, "mbappe");
        System.out.println(songs);
        shuffleList((List<String>) songs);
        System.out.println(songs);
     /*   System.out.println("************************");
        ArrayList<String> songs2 = new ArrayList<>();
        songs2.add(songs.get(0));
        songs2.add(songs.get(1));
        songs2.add(songs.get(2));
        System.out.println(songs2);

        ArrayList<String> songs3 = new ArrayList<>();
        songs3.add(songs.get(3));
        songs3.add(songs.get(4));
        System.out.println(songs3);

*/
    }

    public static void shuffleList(List<String> songs) {

        int n = songs.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(songs, i, change);
        }
    }

    public static void swap(List<String> songs, int i, int change) {
        String helper = songs.get(i);
        songs.set(i, songs.get(change));
        songs.set(change, helper);
    }
}

package thirdpackage;

import java.io.IOException;
import java.sql.*;

import static thirdpackage.MyOwnFileReader.getPropertyFile;

public class MySqlParameter1 {
    public static int firstPara(String query) throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        String username = getPropertyFile("src/thirdpackage/config4.properties", "username");
        String password = getPropertyFile("src/thirdpackage/config4.properties", "password");
        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        int resultSet = statement.executeUpdate(query);
        return resultSet;
    }
    public static ResultSet firstPara2(String query) throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        String username = getPropertyFile("src/thirdpackage/config4.properties", "username");
        String password = getPropertyFile("src/thirdpackage/config4.properties", "password");
        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet;
    }


    public static void getValue(ResultSet resultSet, String columnName) throws SQLException {
        while (resultSet.next()) {
            System.out.println(resultSet.getString(columnName));
        }
    }
}
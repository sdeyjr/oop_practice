package classwork;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFileReader {

    public static void main(String[] args) throws IOException {
/*
        Properties properties = new Properties();

        FileInputStream fs = new FileInputStream("src/classwork/Config.properties");
        properties.load(fs);

        String username = properties.getProperty("username");
        System.out.println(username);

*/

        Properties properties2 = new Properties();

        FileInputStream fs2 = new FileInputStream("src/classwork/Config2.properties");
        properties2.load(fs2);

        String username2 = properties2.getProperty("mysqlusername");
        System.out.println(username2);
        String pass2 = properties2.getProperty("mysqlpass");
        System.out.println(pass2);


    }
}

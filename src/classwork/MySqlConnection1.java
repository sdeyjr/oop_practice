package classwork;

import java.io.IOException;
import java.sql.*;

import static classwork.FileReaderUtils.getPropertyOfFile;

public class MySqlConnection1 {
    public static void main(String[] args) throws SQLException, IOException {

        String valueOfUsername = FileReaderUtils.getPropertyOfFile("src/classwork/Config2.properties", "username");
        String valueOfPass = FileReaderUtils.getPropertyOfFile("src/classwork/Config2.properties", "pass");

        String url = "jdbc:mysql://localhost:3306/classicmodels";
        Connection connection = DriverManager.getConnection(url, valueOfUsername, valueOfPass);

        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("select * from customers");

        while (rs.next()) {
            System.out.println(rs.getString("customerName"));
        }
    }
}

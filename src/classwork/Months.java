package classwork;

public enum Months {

    JANUARY(1), FEBRUARY(2), MARCH(3), APRIL(4), JUNE(5);

    private final int shortForm;

    Months(int shortForm) {
        this.shortForm = shortForm;
    }


    public int getNUmberOfMonth() {
        return shortForm;
    }
}
